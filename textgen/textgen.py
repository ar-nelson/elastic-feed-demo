from random_username.generate import generate_username
from aitextgen import aitextgen
from pymongo import MongoClient
from elasticsearch7 import Elasticsearch
from bson.objectid import ObjectId
from datetime import datetime
import logging
import os
import random
import signal
import time

MONGODB_URL = os.environ["TEXTGEN_MONGODB_URL"]
ELASTIC_URL = os.environ["TEXTGEN_ELASTIC_URL"]

ai = aitextgen(model="minimaxir/hacker-news")

def generate_users(db, count=100):
    existing = [x["name"] for x in db.users.find({})]
    if (len(existing) > 0):
        return existing
    logging.info(f"Generating {count} usernames")
    new_users = generate_username(count)
    db.users.insert_many([{ "_id": ObjectId(), "name": name } for name in new_users])
    return new_users

def generate_post(db, es, users):
    author = random.choice(users)
    content = ai.generate_one(max_length=512)
    document = { "_id": ObjectId(), "author": author, "content": content, "createdAt": datetime.now() }
    db.posts.insert_one(document)
    es.create(index = "posts", id = document["_id"], document = { "author": author, "content": content })

# https://stackoverflow.com/a/70131496/548027
# you've been hit by, you've been struck by
class SmoothCriminal:
  kill_now = False
  def __init__(self):
    signal.signal(signal.SIGINT, self.exit_gracefully)
    signal.signal(signal.SIGTERM, self.exit_gracefully)

  def exit_gracefully(self, signum, frame):
    logging.warning("Shutting down AI text generator")
    self.kill_now = True

if __name__ == "__main__":
    killer = SmoothCriminal()
    with MongoClient(MONGODB_URL) as mongo_client:
        db = mongo_client["elastic_feed_demo"]
        es = Elasticsearch(ELASTIC_URL)
        users = generate_users(db)
        logging.info("Sleeping for 30s to wait for elastic...")
        time.sleep(30)
        logging.info("Generating posts with GPT-2...")
        try:
            while not killer.kill_now:
                generate_post(db, es, users)
                time.sleep(1)
        except KeyboardInterrupt:
            logging.warning("Interrupted!")

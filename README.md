# ElasticSearch Feed Demo

A small project for Chatmeter, to learn and demonstrate a basic setup for Play Framework, MongoDB, and ElasticSearch.

This is a simulated "Hacker News" homepage that continually generates new fake headlines via AI. It supports full-text search over these headlines, via ElasticSearch. It also supports pagination. And that's it!

## How to Run

```
docker-compose up
```

Give it a while to boot up and start generating headlines (it waits 30 seconds, then generates 1 per second).

Then visit `localhost:9000`.

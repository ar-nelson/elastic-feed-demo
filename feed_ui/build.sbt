name := """feed_ui"""
organization := "com.chatmeter"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.13.10"

libraryDependencies += guice
libraryDependencies += ws
libraryDependencies += "org.scalatestplus.play" %% "scalatestplus-play" % "5.0.0" % Test
libraryDependencies += "com.typesafe.play" %% "play-json-joda" % "2.8.2"
libraryDependencies += "org.mongodb.scala" %% "mongo-scala-driver" % "4.8.0"

// Adds additional packages into Twirl
//TwirlKeys.templateImports += "com.chatmeter.controllers._"

// Adds additional packages into conf/routes
// play.sbt.routes.RoutesKeys.routesImport += "com.chatmeter.binders._"

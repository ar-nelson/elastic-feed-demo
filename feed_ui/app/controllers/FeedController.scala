package controllers

import org.bson.types.ObjectId

import javax.inject._
import play.api._
import play.api.mvc._
import services.{ElasticService, MongoService}

import scala.concurrent.ExecutionContext

/**
 * This controller creates an `Action` to handle HTTP requests to the
 * application's home page.
 */
@Singleton
class FeedController @Inject()(
  val controllerComponents: ControllerComponents,
  val mongo: MongoService,
  val elastic: ElasticService
)(implicit executor: ExecutionContext) extends BaseController {

  /**
   * Create an Action to render an HTML page.
   *
   * The configuration in the `routes` file means that this method
   * will be called when the application receives a `GET` request with
   * a path of `/`.
   */
  def index(start: Option[String]): Action[AnyContent] = Action.async { implicit request: Request[AnyContent] =>
    for ((posts, next) <- mongo.posts(start.map(new ObjectId(_)))) yield Ok(views.html.index(posts, next))
  }

  def user(user: String, start: Option[String]): Action[AnyContent] = Action.async { implicit request: Request[AnyContent] =>
    for ((posts, next) <- mongo.postsByUser(user, start.map(new ObjectId(_)))) yield Ok(views.html.user(user, posts, next))
  }

  def search(term: String, start: Option[String]): Action[AnyContent] = Action.async { implicit request: Request[AnyContent] =>
    for {
      ids <- elastic.search(term)
      posts <- mongo.postsByIds(ids)
    } yield Ok(views.html.search(term, posts))
  }
}

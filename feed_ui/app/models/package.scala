import org.bson.types.ObjectId
import org.joda.time.DateTime
import play.api.libs.json.{Format, JodaReads, JodaWrites, JsError, JsResult, JsString, JsSuccess, JsValue}

import scala.util.Try

package object models {
  /** https://stackoverflow.com/a/52863011 */
  implicit object ObjectIdFormat extends Format[ObjectId] {

    def writes(objectId: ObjectId): JsValue = JsString(objectId.toString)
    def reads(json: JsValue): JsResult[ObjectId] = json match {
      case JsString(x) => {
        val maybeOID: Try[ObjectId] = Try{new ObjectId(x)}
        if(maybeOID.isSuccess) JsSuccess(maybeOID.get) else {
          JsError("Expected ObjectId as JsString")
        }
      }
      case _ => JsError("Expected ObjectId as JsString")
    }
  }

  private val pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
  implicit val dateFormat = Format[DateTime](JodaReads.jodaDateReads(pattern), JodaWrites.jodaDateWrites(pattern))
}

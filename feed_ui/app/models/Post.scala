package models

import org.bson.types.ObjectId
import org.joda.time.DateTime
import play.api.libs.json.{Format, Json}

case class Post(_id: ObjectId, author: String, content: String, createdAt: DateTime)

object Post {
  implicit val format: Format[Post] = Json.using[Json.WithDefaultValues].format[Post]
}
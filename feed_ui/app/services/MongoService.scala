package services

import com.google.inject.{Inject, Singleton}
import models.Post
import org.bson.{BsonReader, BsonWriter}
import org.bson.codecs.{Codec, DecoderContext, EncoderContext}
import org.bson.types.ObjectId
import org.bson.codecs.configuration.CodecRegistries.{fromCodecs, fromProviders, fromRegistries}
import org.joda.time.DateTime
import org.mongodb.scala.MongoClient.DEFAULT_CODEC_REGISTRY
import org.mongodb.scala.bson.codecs.Macros._
import org.mongodb.scala._
import org.mongodb.scala.bson.conversions
import org.mongodb.scala.model.Filters._
import org.mongodb.scala.model.Filters.{eq => _eq}
import org.mongodb.scala.model.Sorts._

import scala.concurrent.{ExecutionContext, Future}
import scala.util.Properties

@Singleton
class MongoService @Inject ()(implicit executor: ExecutionContext) {
  private object JodaCodec extends Codec[DateTime] {
    override def decode(bsonReader: BsonReader, decoderContext: DecoderContext): DateTime = new DateTime(bsonReader.readDateTime())
    override def encode(bsonWriter: BsonWriter, t: DateTime, encoderContext: EncoderContext): Unit = bsonWriter.writeDateTime(t.getMillis)
    override def getEncoderClass: Class[DateTime] = classOf[DateTime]
  }

  private val codecRegistry = fromRegistries(fromCodecs(JodaCodec), fromProviders(classOf[Post]), DEFAULT_CODEC_REGISTRY)
  private val client = MongoClient(Properties.envOrElse("FEEDUI_MONGODB_URL",
    throw new IllegalStateException("Required environment variable FEEDUI_MONGODB_URL is not defined")
  ))
  private val db = client.getDatabase("elastic_feed_demo").withCodecRegistry(codecRegistry)
  private val postsCollection: MongoCollection[Post] = db.getCollection("posts")

  def posts(startAt: Option[ObjectId] = None, maxPosts: Int = 50): Future[(Seq[Post], Option[ObjectId])] =
    for (
      results <- postsCollection
        .find(startAt.fold[conversions.Bson](Document())(id => lte("_id", id)))
        .sort(orderBy(descending("_id")))
        .limit(maxPosts + 1)
        .toFuture()
    ) yield if (results.length > maxPosts) (results.dropRight(1), Some(results.last._id)) else (results, None)

  def postsByUser(user: String, startAt: Option[ObjectId] = None, maxPosts: Int = 50): Future[(Seq[Post], Option[ObjectId])] =
    for (
      results <- postsCollection
        .find(startAt.fold(_eq("author", user))(id => and(_eq("author", user), lt("_id", id))))
        .sort(orderBy(descending("_id")))
        .limit(maxPosts + 1)
        .toFuture()
    ) yield if (results.length > maxPosts) (results.dropRight(1), Some(results.last._id)) else (results, None)

  def postsByIds(ids: Seq[ObjectId]): Future[Seq[Post]] =
    postsCollection
      .find(Document("_id" -> Document("$in" -> ids)))
      .sort(orderBy(descending("_id")))
      .toFuture()
}

package services

import com.google.inject.{Inject, Singleton}
import org.bson.types.ObjectId
import play.api.libs.ws._
import models.ObjectIdFormat

import java.net.URLEncoder
import scala.concurrent.{ExecutionContext, Future}
import scala.util.Properties

@Singleton
class ElasticService @Inject() (ws: WSClient)(implicit executor: ExecutionContext) {
  private val baseUrl = Properties.envOrElse("FEEDUI_ELASTIC_URL",
    throw new IllegalStateException("Required environment variable FEEDUI_ELASTIC_URL is not defined")
  )

  def search(term: String): Future[Seq[ObjectId]] =
    for (
      // can't use withQueryStringParameters here because the : must be unescaped
      rsp <- ws.url(s"$baseUrl/posts/_search?q=content:${URLEncoder.encode(term, "utf-8")}").get()
    ) yield (rsp.json \ "hits" \ "hits" \\ "_id").map(_.as[ObjectId]).toList
}
